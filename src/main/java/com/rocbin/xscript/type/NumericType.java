package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public abstract class NumericType<T extends Number> implements Type<Number> {

    public NumericType() {
    }


    @Override
    public Number getValue() {
        throw new UnsupportedOperationException("unsupported abstract method call.");
    }
}
