package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public interface Type<T> {

    java.lang.String getName();

    Class<? extends T> getJavaType();

    T convertValue(java.lang.String value) throws RuntimeException;

    T getValue();

    void setValue(T value);
}
