package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class Integer extends NumericType<java.lang.Integer> {

    java.lang.Integer value;

    public Integer() {
    }

    public Integer(java.lang.Integer value) {
        this.value = value;
    }

    @Override
    public java.lang.String getName() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public Class<java.lang.Integer> getJavaType() {
        return java.lang.Integer.TYPE;
    }

    @Override
    public java.lang.Integer convertValue(java.lang.String value) throws RuntimeException {
        return java.lang.Integer.valueOf(value);
    }

    @Override
    public java.lang.Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Number value) {
        this.value = value == null ? null : value.intValue();
    }

}
