package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 4.0
 */
public class String implements Type<java.lang.String> {

    java.lang.String value;

    @Override
    public java.lang.String getName() {
        return "string";
    }

    @Override
    public Class<? extends java.lang.String> getJavaType() {
        return java.lang.String.class;
    }

    @Override
    public java.lang.String convertValue(java.lang.String value) throws RuntimeException {
        return value;
    }

    @Override
    public java.lang.String getValue() {
        return value;
    }

    @Override
    public void setValue(java.lang.String value) {
        this.value = value;
    }
}
