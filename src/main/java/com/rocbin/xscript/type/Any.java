package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class Any implements Type<Object> {

    Object value;

    @Override
    public java.lang.String getName() {
        return "any";
    }

    @Override
    public Class<?> getJavaType() {
        return Object.class;
    }

    @Override
    public java.lang.String convertValue(java.lang.String value) throws RuntimeException {
        return value;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void setValue(Object value) {
        this.value = value;
    }
}
