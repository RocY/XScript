package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class Double extends NumericType<java.lang.Double> {

    java.lang.Double value;

    public Double() {
    }

    public Double(java.lang.Double value) {
        this.value = value;
    }

    @Override
    public java.lang.String getName() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public Class<? extends Number> getJavaType() {
        return java.lang.Double.TYPE;
    }

    @Override
    public java.lang.Double convertValue(java.lang.String value) throws RuntimeException {
        return java.lang.Double.valueOf(value);
    }

    @Override
    public Number getValue() {
        return value;
    }

    @Override
    public void setValue(Number value) {
        this.value = value.doubleValue();
    }
}
