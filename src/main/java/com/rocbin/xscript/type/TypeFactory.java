package com.rocbin.xscript.type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 4.0
 */
public class TypeFactory {

    private TypeFactory() {
    }

    public static Type getType(java.lang.String typeName) {
        switch (typeName) {
            case "integer": {
                return new Integer();
            }
            case "double": {
                return new Double();
            }
            case "string": {
                return new String();
            }
            default:
                throw new RuntimeException("不支持的XScript数据类型：" + typeName);
        }
    }
}
