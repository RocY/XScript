package com.rocbin.xscript;

import com.rocbin.xscript.context.SubScope;
import com.rocbin.xscript.directive.DeclareVariableDirective;
import com.rocbin.xscript.directive.PrintLineDirective;
import com.rocbin.xscript.directive.SumDirective;
import com.rocbin.xscript.type.Double;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
//        <sub name="add">
//          <code>
//              <var name="sum" type="integer" default="0"/>
//              <sum args="num1,num2,num3" out="sum"/>
//              <println template="输出和=%1$s" args="sum"/>
//          </code>
//        </sub>
        SubScope sub = new SubScope("add");
        sub.declareVariable("sum", new Double(0.0));
        sub.declareVariable("num1", new Double(1.0));
        sub.declareVariable("num2", new Double(2.0));
        sub.addDirective(new DeclareVariableDirective("num3", "double", "10.0"));
        sub.addDirective(new SumDirective("num1,num2,num3", "sum"));
        sub.addDirective(new PrintLineDirective("输出和=%1$s", "sum"));

        long start = System.currentTimeMillis();
        sub.execute();
        long spend = System.currentTimeMillis() - start;
        System.out.println(sub.getScopeName() + " 执行耗时ms=" + spend);
    }
}
