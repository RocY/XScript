package com.rocbin.xscript.directive;

import com.rocbin.xscript.type.Type;
import com.rocbin.xscript.type.TypeFactory;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class DeclareVariableDirective extends AbstractDirective {

    String variableName;
    String type;
    String defaultValue;

    public DeclareVariableDirective(String variableName, String type, String defaultValue) {
        this.variableName = variableName;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public DeclareVariableDirective() {
        directiveName = "var";
    }

    public void execute() {
        Object value = null;
        Type type = TypeFactory.getType(this.type);
        if(defaultValue != null) {
            value = type.convertValue(defaultValue);
            type.setValue(value);
        }
        getScope().getThisContext().declareVariable(variableName, type);
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
