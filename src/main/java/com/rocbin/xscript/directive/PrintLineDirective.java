package com.rocbin.xscript.directive;

import com.rocbin.xscript.context.Context;
import com.rocbin.xscript.type.Type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class PrintLineDirective extends AbstractDirective {

    String template;
    String args;

    public PrintLineDirective() {
        directiveName = "println";
    }

    public PrintLineDirective(String template, String args) {
        this();
        this.template = template;
        this.args = args;
    }

    @Override
    public void execute() {
        Context thisContext = getScope().getThisContext();
        Context superContext = getScope().getSuperContext();
        String[] arguments = args.split(",");
        Type[] variables = new Type[arguments.length];
        int index = 0;
        for (String argument : arguments) {
            Type var = thisContext.getVariable(argument);
            if (var == null && superContext != null) {
                var = superContext.getVariable(argument);
            }
            if (var == null) {
                throw new RuntimeException(String.format("XScript Error: variable %1$1 is undefined!", argument));
            }
            variables[index++] = var;
        }
        Object[] nativeValues = new Object[variables.length];
        index = 0; // reset
        for (Type variable : variables) {
            nativeValues[index++] = variable.getValue();
        }
        if (template == null || template.isEmpty()) {
            for (Object nativeValue : nativeValues) {
                System.out.print(nativeValue);
                if (nativeValues.length > 1) {
                    System.out.print(","); // array split output
                }
            }
            System.out.println();
        } else {
            System.out.println(String.format(template, nativeValues));
        }
    }
}
