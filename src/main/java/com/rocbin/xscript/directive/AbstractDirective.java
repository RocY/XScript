package com.rocbin.xscript.directive;

import com.rocbin.xscript.context.Scope;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 4.0
 */
public abstract class AbstractDirective implements IDirective {
    String directiveName;  // 指令名称
    Scope scope;  // 指令作用域，this=当前 super=上级

    public String getDirectiveName() {
        return directiveName;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }
}
