package com.rocbin.xscript.directive;

import com.rocbin.xscript.context.Scope;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public interface IDirective {

    void execute();

    Scope getScope();

    void setScope(Scope scope);
}
