package com.rocbin.xscript.directive;

import com.rocbin.xscript.context.Context;
import com.rocbin.xscript.type.Any;
import com.rocbin.xscript.type.Type;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * XScript Java Method Call Directive
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class NativeCallDirective extends AbstractDirective {

    String className;
    String method;
    String args;
    String out;

    public NativeCallDirective() {
        directiveName = "native";
    }

    public NativeCallDirective(String className, String method, String args, String out) {
        this.className = className;
        this.method = method;
        this.args = args;
        this.out = out;
    }

    @Override
    public void execute() {
        try {
            Class<?> clazz = Class.forName(className);
            Method[] methods = clazz.getMethods();
            Object instance = null;
            Method fun = null;
            Class<?> returnType = null;
            for (Method method : methods) {
                if (this.method.equals(method.getName())) {
                    returnType = method.getReturnType();
                    int fnModifiers = method.getModifiers();
                    if (!Modifier.isStatic(fnModifiers)) {
                        // create instance for clazz
                        instance = clazz.newInstance();
                    }
                    if (Modifier.isPrivate(fnModifiers)) {
                        // private method set access true!
                        method.setAccessible(true);
                    }
                    fun = method;
                    break;
                }
            }
            if (fun == null) {
                throw new RuntimeException(String.format("XScript Error: native call method %1$s not found!", method));
            }
            // handle variables
            Object[] variables = getVariables();
            // invoke native function
            Object invokeValue = fun.invoke(instance, variables);
            if (returnType != Void.class) {
                Any outputVariable = new Any();
                outputVariable.setValue(invokeValue);
                getScope().getThisContext().declareVariable(out, outputVariable);
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("XScript Error: native call class not found!", e);
        } catch (InstantiationException e) {
            throw new RuntimeException("XScript Error: native call class new instance failure!", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("XScript Error: native call class new instance access failure!", e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("XScript Error: native call method throw an exception!", e);
        }
    }

    private Object[] getVariables() {
        List<Object> variableList = new ArrayList<>();
        Context thisContext = getScope().getThisContext();
        Context superContext = getScope().getSuperContext();
        for (String arg : args.split(",")) {
            if (arg.trim().length() == 0) {
                continue;
            }
            Type variable = thisContext.getVariable(arg);
            if (variable == null) {
                variable = superContext.getVariable(arg);
            }
            if (variable == null) {
                throw new RuntimeException(String.format("XScript Error: native call the variable '%1$s' not found", arg));
            }
            variableList.add(variable.getValue());
        }
        return variableList.toArray();
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }
}
