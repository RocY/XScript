package com.rocbin.xscript.directive;

import com.rocbin.xscript.context.Context;
import com.rocbin.xscript.type.Double;
import com.rocbin.xscript.type.NumericType;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class SumDirective extends AbstractDirective {

    String args;
    String out;

    public SumDirective() {
        directiveName = "sum";
    }

    public SumDirective(String args, String out) {
        this();
        this.args = args;
        this.out = out;
    }

    @Override
    public void execute() {
        Context thisContext = getScope().getThisContext();
        Context superContext = getScope().getSuperContext();
        String[] arguments = args.split(",");
        NumericType<java.lang.Double> outVariable = null;
        if ((outVariable = thisContext.getVariable(out)) == null) {
            outVariable = superContext.getVariable(out);
        }
        if (outVariable == null) {
            outVariable = new Double();
            thisContext.declareVariable(out, outVariable);
            outVariable.setValue(0.0);
        }
        Number outVariableValue = outVariable.getValue();
        for (String argument : arguments) {
            NumericType<Number> var = thisContext.getVariable(argument);
            if (var == null && superContext != null) {
                var = superContext.getVariable(argument);
            }
            if (var == null) {
                throw new RuntimeException(String.format("XScript Error: variable %1$s is undefined", argument));
            }
            java.lang.Double value = (java.lang.Double) var.getValue();
            outVariableValue = outVariableValue.doubleValue() + value;
        }
        outVariable.setValue(outVariableValue);
    }
}
