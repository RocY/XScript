package com.rocbin.xscript.context;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public interface Scope {

    String getScopeName();

    Context getThisContext();

    Context getSuperContext();
}
