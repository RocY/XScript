package com.rocbin.xscript.context;

import com.rocbin.xscript.type.Type;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public interface Context {

    void declareVariable(String variableName, Type value);

    <T extends Type> T getVariable(String variableName);

    boolean deleteVariable(String variableName);


}
