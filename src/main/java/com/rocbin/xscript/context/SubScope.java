package com.rocbin.xscript.context;

import com.rocbin.xscript.directive.IDirective;
import com.rocbin.xscript.type.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sub function
 *
 * @author Rocbin
 * @version 1.0
 * @since 1.0
 */
public class SubScope implements Scope, Context, IDirective {
    String name;

    Map<String, Type> variableContext = new HashMap<>();

    List<IDirective> codeList = new ArrayList<>();

    public SubScope() {
    }

    public SubScope(String name) {
        this.name = name;
    }

    public void addDirective(IDirective directive) {
        directive.setScope(this);
        codeList.add(directive);
    }

    @Override
    public String getScopeName() {
        return "sub " + name;
    }

    @Override
    public Context getThisContext() {
        return this;
    }

    @Override
    public Context getSuperContext() {
        return this; // TODO 暂未实现全局 Context
    }

    @Override
    public void declareVariable(String variableName, Type value) {
        if (value == null) {
            throw new RuntimeException("XScript Error: declare value the value cannot be null!");
        }
        variableContext.put(variableName, value);
    }

    @Override
    public <T extends Type> T getVariable(String variableName) {
        return (T) variableContext.get(variableName);
    }

    @Override
    public boolean deleteVariable(String variableName) {
        return variableContext.remove(variableName) != null;
    }

    @Override
    public void execute() {
        for (IDirective directive : codeList) {
            try {
                directive.execute();
            } catch (Exception e) {
                throw new RuntimeException("XScript Error: " + getScopeName() + " execute has an exception", e);
            }
        }
    }

    @Override
    public Scope getScope() {
        return this;
    }

    @Override
    public void setScope(Scope scope) {
        // TODO 未实现 sub function 作用域
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Type> getVariableContext() {
        return variableContext;
    }

    public void setVariableContext(Map<String, Type> variableContext) {
        this.variableContext = variableContext;
    }

    public List<IDirective> getCodeList() {
        return codeList;
    }

    public void setCodeList(List<IDirective> codeList) {
        this.codeList = codeList;
    }
}
