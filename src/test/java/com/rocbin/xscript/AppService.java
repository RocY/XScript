package com.rocbin.xscript;

import com.rocbin.xscript.context.SubScope;
import com.rocbin.xscript.directive.DeclareVariableDirective;
import com.rocbin.xscript.directive.NativeCallDirective;
import com.rocbin.xscript.directive.PrintLineDirective;

/**
 * Class Description
 *
 * @author Rocbin
 * @version 1.0
 * @since 4.0
 */
public class AppService {

    public static void main(String[] args) {
//        <sub>
//          <code>
//              <var name="result" type="string" default=""/>
//              <native class="com.rocbin.xscript.AppService" method="getName" args="", out="result"/>
//              <println template="native 调用输出结果是%1$s" args="result"/>
//          </code>
//        </sub>
        SubScope sub = new SubScope("nativeDemo");
        sub.addDirective(new DeclareVariableDirective("result", "string", ""));
//        NativeCallDirective nativeCall = new NativeCallDirective("com.rocbin.xscript.AppService", "getName", "", "result");
        sub.addDirective(new DeclareVariableDirective("person", "string", "Rocbin"));
        NativeCallDirective nativeCall = new NativeCallDirective("com.rocbin.xscript.AppService", "getName2", "person", "result");
        sub.addDirective(nativeCall);
        sub.addDirective(new PrintLineDirective("native 调用输出结果是%1$s", "result"));
        long start = System.currentTimeMillis();
        sub.execute();
        System.out.println(System.currentTimeMillis() - start);
    }

    public String getName() {
        return "XScript: hello native";
    }

    public String getName2(String person) {
        return String.format("Hello %1$s !", person);
    }
}
